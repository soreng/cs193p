//
//  main.m
//  StanfordEDU
//
//  Created by Stephan Kristiansen on 21.06.13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CardGameAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CardGameAppDelegate class]));
    }
}
