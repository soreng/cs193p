//
//  SetMatchingGame.m
//  StanfordEDU
//
//  Created by Stephan Kristiansen on 14.07.13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import "SetMatchingGame.h"
#import "Config.h"

@interface SetMatchingGame ()

@property (nonatomic, readwrite) int score;
@property (strong, nonatomic) NSMutableArray *lastResultsMessages;

@end

@implementation SetMatchingGame

- (NSMutableArray *)flipCardAtIndex:(NSUInteger)index
{
    NSMutableArray *returnResult = [[NSMutableArray alloc] init];
    NSMutableArray *matchArray = [[NSMutableArray alloc] init];
    Card *card = [self cardAtIndex:index];
    if(!card.isUnplayable){
        if(!card.isFaceUp){
            for(Card *otherCard in self.cards){
                if(otherCard.isFaceUp && !otherCard.isUnplayable){
                    [matchArray addObject:otherCard];
                }
            }
            
            self.score -= FLIP_COST;
            
            if([matchArray count] == 2){
                int matchScore = [card match:matchArray];
                if(matchScore){
                    int totalScore = matchScore * MATCH_BONUS;
                    self.score += totalScore;
                    
                    [returnResult insertObject:[[NSNumber alloc] initWithInt:totalScore] atIndex:0];
                    [returnResult insertObject:card atIndex:1];
                    
                    for(Card *otherCard in matchArray){
                        otherCard.unplayable = YES;
                        [returnResult addObject:otherCard];
                    }
                    
                    card.unplayable = YES;
                }
                else{
                    int totalscore = -MISMATCH_PENALTY;
                    self.score -= MISMATCH_PENALTY;
                    
                    [returnResult insertObject:[[NSNumber alloc] initWithInt:totalscore] atIndex:0];
                    [returnResult insertObject:card atIndex:1];
                    
                    for(Card *otherCard in matchArray){
                        otherCard.faceUp = NO;
                        [returnResult addObject:otherCard];
                    }
                    
                    card.faceUp = YES;
                    
                }
            }
            else{
                [returnResult insertObject:card atIndex:0];
            }
        }
        
        card.faceUp = !card.faceUp;
    }
    
    return returnResult;
}

@end
