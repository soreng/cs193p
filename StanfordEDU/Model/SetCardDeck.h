//
//  SetCardDeck.h
//  StanfordEDU
//
//  Created by Stephan Kristiansen on 14.07.13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import "Deck.h"

@interface SetCardDeck : Deck

@end
