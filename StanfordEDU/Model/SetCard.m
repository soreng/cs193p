//
//  SetCard.m
//  StanfordEDU
//
//  Created by Stephan Kristiansen on 12.07.13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import "SetCard.h"

@implementation SetCard

/*
 They all have the same number, or they have three different numbers.
 They all have the same symbol, or they have three different symbols.
 They all have the same shading, or they have three different shadings.
 They all have the same color, or they have three different colors.
 
 The rules of Set are summarized by: If you can sort a group of three cards into "Two of ____ and one of _____," then it is not a set.
 */

- (int)match:(NSArray *)otherCards
{
    int score = 0;
    if([otherCards count] == 2){
        SetCard *otherCard1 = (SetCard *)otherCards[0];
        SetCard *otherCard2 = (SetCard *)otherCards[1];
        
        BOOL isSet = false;
        if((self.number == otherCard1.number && self.number == otherCard2.number) || (self.number != otherCard1.number && self.number != otherCard2.number && otherCard1.number != otherCard2.number)){
            
            if(([self.symbol isEqualToString:otherCard1.symbol] && [self.symbol isEqualToString:otherCard2.symbol]) || (![self.symbol isEqualToString:otherCard1.symbol] && ![self.symbol isEqualToString:otherCard2.symbol] && ![otherCard1.symbol isEqualToString:otherCard2.symbol])){
                
                if(([self.shading isEqualToString:otherCard1.shading] && [self.shading isEqualToString:otherCard2.shading]) || (![self.shading isEqualToString:otherCard1.shading] && ![self.shading isEqualToString:otherCard2.shading] && ![otherCard1.shading isEqualToString:otherCard2.shading])){
                    
                    if(([self.color isEqualToString:otherCard1.color] && [self.color isEqualToString:otherCard2.color]) || (![self.color isEqualToString:otherCard1.color] && ![self.color isEqualToString:otherCard2.color] && ![otherCard1.color isEqualToString:otherCard2.color])){
                        
                        isSet = true;
                        
                        score = 8;
                    }
                }
            }
        }
    }
    
    return score;
}

+ (NSArray *)symbolsStrings{
    return @[@"▲",@"▲▲",@"▲▲▲", @"●",@"●●",@"●●●", @"■", @"■■", @"■■■"];
}

+ (NSArray *)validShadings{
    return @[@"open",@"striped",@"solid"];
}

+ (NSArray *)validNumbers{
    return @[@1,@2,@3];
}

+ (NSArray *)validColors{;    
    return @[@"red",@"green",@"purple"];
}


- (NSString *)contents
{
    return self.symbol;
}
 

@end
