//
//  PlayingCardDeck.h
//  StanfordEDU
//
//  Created by Stephan Kristiansen on 21.06.13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface PlayingCardDeck : Deck

@end
