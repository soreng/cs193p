//
//  SetCard.h
//  StanfordEDU
//
//  Created by Stephan Kristiansen on 12.07.13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import "PlayingCard.h"

@interface SetCard : Card

@property (strong,nonatomic) NSString *symbol;
@property (nonatomic) NSUInteger number;
@property (strong,nonatomic) NSString *shading;
@property (strong,nonatomic) NSString *color;

+ (NSArray *)symbolsStrings;
+ (NSArray *)validShadings;
+ (NSArray *)validNumbers;
+ (NSArray *)validColors;

@end
