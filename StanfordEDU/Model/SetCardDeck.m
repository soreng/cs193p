//
//  SetCardDeck.m
//  StanfordEDU
//
//  Created by Stephan Kristiansen on 14.07.13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import "SetCardDeck.h"
#import "SetCard.h"

@implementation SetCardDeck

- (id)init
{
    self = [super init];
    if(self){
        for(NSString *symbol in [SetCard symbolsStrings]){
            for(NSString *shading in [SetCard validShadings]){
                for(NSString *color in [SetCard validColors]){
                    SetCard *card = [[SetCard alloc] init];
                    card.color = color;
                    card.shading = shading;
                    card.symbol = symbol;
                    card.number = [symbol length];
                    [self addCard:card atTop:YES];
                }
            }
        }
    }
    return self;
}

@end
