//
//  PlayingCard.h
//  StanfordEDU
//
//  Created by Stephan Kristiansen on 21.06.13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface PlayingCard : Card

@property (strong,nonatomic) NSString *suit;
@property (nonatomic) NSUInteger rank;

+ (NSArray *)validSuits;
+ (NSArray *)rankStrings;
+ (NSUInteger)maxRank;
@end
