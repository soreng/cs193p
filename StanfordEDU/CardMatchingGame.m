//
//  CardMatchingGame.m
//  StanfordEDU
//
//  Created by Stephan Kristiansen on 23.06.13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import "CardMatchingGame.h"
#import "Config.h"

@interface CardMatchingGame ()


@property (nonatomic, readwrite) int score;
@property (strong, nonatomic) NSMutableArray *lastResultsMessages;

@end

@implementation CardMatchingGame

- (NSMutableArray *)cards
{
    if(!_cards) _cards = [[NSMutableArray alloc] init];
    return _cards;
}

- (NSMutableArray *)lastResultsMessages
{
    if(!_lastResultsMessages) _lastResultsMessages = [[NSMutableArray alloc] init];
    return _lastResultsMessages;
}

- (id)initWithCardCount:(NSUInteger)cardCount usingDeck:(Deck *)deck
{
    self = [super init];
    if(self){
        for(int i=0; i<cardCount; i++)
        {
            Card *card = [deck drawRandomCard];
            if(!card){
                self = nil;
            } else{
                self.cards[i] = card;
            }
        }
    }
    return self;
}
- (Card *)cardAtIndex:(NSUInteger)index
{
    return index < [self.cards count] ? self.cards[index] : nil;
}

- (NSMutableArray *)flipCardAtIndex:(NSUInteger)index
{
    NSMutableArray *returnResult = [[NSMutableArray alloc] init];
    Card *card = [self cardAtIndex:index];
    if(!card.isUnplayable){
        if(!card.isFaceUp){
            int count = 0;
            for(Card *otherCard in self.cards){
                if(otherCard.isFaceUp && !otherCard.isUnplayable){
                    count++;
                    NSArray *matchArray = [[NSArray alloc] initWithObjects:otherCard, nil];
                    int matchScore = [card match:matchArray];
                    if(matchScore){
                        otherCard.unplayable = YES;
                        card.unplayable = YES;
                        int totalScore = matchScore * MATCH_BONUS;
                        
                        self.score += totalScore;
                        
                        [returnResult insertObject:[[NSNumber alloc] initWithInt:totalScore] atIndex:0];
                        [returnResult insertObject:card atIndex:1];
                        [returnResult insertObject:otherCard atIndex:2];
                                                
                    } else{
                        unsigned int totalscore = -MISMATCH_PENALTY;
                        self.score -= MISMATCH_PENALTY;
                        otherCard.faceUp = NO;
                        
                        [returnResult insertObject:[[NSNumber alloc] initWithInt:totalscore] atIndex:0];
                        [returnResult insertObject:card atIndex:1];
                        [returnResult insertObject:otherCard atIndex:2];
                    }
                }
            }
            self.score -= FLIP_COST;
            
            if(count == 0){
              [returnResult insertObject:card atIndex:0];
            }            
        }
        
        card.faceUp = !card.faceUp;
    }
    
    return returnResult;
}

- (id)getLastMatchMessage{
    int lastIndex = [self.lastResultsMessages count];
    if(lastIndex){
        return [self.lastResultsMessages objectAtIndex:(lastIndex - 1)];
    }
    
    return @"";
}

@end
