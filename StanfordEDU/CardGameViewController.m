//
//  CardGameViewController.m
//  StanfordEDU
//
//  Created by Stephan Kristiansen on 21.06.13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import "CardGameViewController.h"
#import "PlayingCardDeck.h"
#import "CardMatchingGame.h"
#import "Config.h"
#import "Card.h"

@interface CardGameViewController ()
@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property (nonatomic) int flipCount;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (strong, nonatomic) CardMatchingGame *game;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastResultMessageLabel;
@property (weak, nonatomic) IBOutlet UIButton *dealButton;
@end

@implementation CardGameViewController

- (CardMatchingGame *)game
{
    if(!_game){
        _game = [[CardMatchingGame alloc] initWithCardCount:[self.cardButtons count] usingDeck:[[PlayingCardDeck alloc] init]];
    }
    
    return _game;
}

- (void)setCardButtons:(NSArray *)cardButtons
{
    _cardButtons = cardButtons;
    [self updateUI];
}

- (void)updateUI{
    UIImage *cardBackImage = [UIImage imageNamed:@"cardback.jpeg"];
    for(UIButton *cardButton in self.cardButtons){
        Card *card = [self.game cardAtIndex:[self.cardButtons indexOfObject:cardButton]];
        
        UIEdgeInsets cardButtonUIEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        [cardButton setImageEdgeInsets:cardButtonUIEdgeInsets];
        
        [cardButton setImage:(card.isFaceUp ? nil : cardBackImage) forState:UIControlStateNormal];
        [cardButton setTitle:card.contents forState:UIControlStateSelected];
        [cardButton setTitle:card.contents forState:UIControlStateSelected|UIControlStateDisabled];
        cardButton.selected = card.isFaceUp;
        cardButton.enabled = !card.isUnplayable;
        cardButton.alpha = card.isUnplayable ? 0.3 : 1.0;
    }
    
    [self.scoreLabel setText:[[NSString alloc] initWithFormat:@"Score: %d", [self.game score]]];
    [self.lastResultMessageLabel setText:[self.game getLastMatchMessage]];
}

- (void)setFlipCount:(int)flipCount{
    _flipCount = flipCount;
    self.flipsLabel.text = [NSString stringWithFormat:@"Flips: %d", self.flipCount];
}

- (IBAction)flipCard:(UIButton *)sender {
    NSMutableArray *resultFromFlip = [self.game flipCardAtIndex:[self.cardButtons indexOfObject:sender]];
    [self generateMatchMessage:resultFromFlip];
    
    self.flipCount++;
    [self updateUI];
}

- (void)generateMatchMessage:(NSMutableArray *)resultFromFlip{
    if(resultFromFlip){
        if([resultFromFlip count] == 3){
            NSNumber *score = (NSNumber *)resultFromFlip[0];
            if([score intValue] > 0){
                NSString *matchMessage = [NSString stringWithFormat:@"Matched %@ and %@ for a total of %@ points", ((Card *)resultFromFlip[1]).contents, ((Card *)resultFromFlip[2]).contents, (NSNumber *)resultFromFlip[0]];
                
                [[self.game lastResultsMessages] addObject:matchMessage];
            }
            else{
                NSString *matchMessage = [NSString stringWithFormat:@"%@ and %@ don't match! %@ points penalty!", ((Card *)resultFromFlip[1]).contents, ((Card *)resultFromFlip[2]).contents, (NSNumber *)resultFromFlip[0]];
                
                [[self.game lastResultsMessages] addObject:matchMessage];
            }
        }
        else{
            NSString *matchMessage = [NSString stringWithFormat:@"Flipped up %@", ((Card *)resultFromFlip[0]).contents];
            [[self.game lastResultsMessages] addObject:matchMessage];
        }
    }
}

- (IBAction)dealGame:(UIButton *)sender{
    UIImage *cardBackImage = [UIImage imageNamed:@"cardback.jpeg"];
    for(UIButton *cardButton in self.cardButtons){
        UIEdgeInsets cardButtonUIEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        [cardButton setImageEdgeInsets:cardButtonUIEdgeInsets];
        [cardButton setImage:cardBackImage forState:UIControlStateNormal];
        cardButton.selected = false;
        cardButton.enabled = true;
        cardButton.alpha = 1.0;
    }
    
    self.game = nil;
    
    [self.scoreLabel setText:[[NSString alloc] initWithFormat:@"Score: %d", [self.game score]]];
    [self.lastResultMessageLabel setText:[self.game getLastMatchMessage]];
    self.flipCount = 0;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
