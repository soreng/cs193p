//
//  CardMatchingGame.h
//  StanfordEDU
//
//  Created by Stephan Kristiansen on 23.06.13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface CardMatchingGame : NSObject

- (id)initWithCardCount:(NSUInteger)cardCount
              usingDeck:(Deck *)deck;

- (NSMutableArray *)flipCardAtIndex:(NSUInteger)index;
- (Card *)cardAtIndex:(NSUInteger)index;
- (NSString *)getLastMatchMessage;

@property (strong, nonatomic) NSMutableArray *cards;
@property (nonatomic, readonly) int score;
@property (nonatomic, readonly) NSMutableArray *lastResultsMessages;

@end
