//
//  SetGameViewController.m
//  StanfordEDU
//
//  Created by Stephan Kristiansen on 12.07.13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import "SetGameViewController.h"
#import "SetMatchingGame.h"
#import "SetCardDeck.h"
#import "SetCard.h"


@interface SetGameViewController ()
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (weak, nonatomic) IBOutlet UILabel *flipLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (nonatomic) int flipCount;
@property (strong, nonatomic) SetMatchingGame *game;
@property (weak, nonatomic) IBOutlet UILabel *lastResultMessageLabel;

@end

@implementation SetGameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (SetMatchingGame *)game
{
    if(!_game){
        _game = [[SetMatchingGame alloc] initWithCardCount:[self.cardButtons count] usingDeck:[[SetCardDeck alloc] init]];
    }
    
    return _game;
}

- (void)generateMatchMessage:(NSMutableArray *)resultFromFlip{
    if(resultFromFlip){
        if([resultFromFlip count] == 1){
            NSString *matchMessage = [NSString stringWithFormat:@"Flipped up %@", ((Card *)resultFromFlip[0]).contents];
            [[self.game lastResultsMessages] addObject:matchMessage];
        }
        else{
            NSNumber *score = (NSNumber *)resultFromFlip[0];
            NSMutableAttributedString *matchMessage = [[NSMutableAttributedString alloc] init];
            
            for(int i=1;i<[resultFromFlip count];i++){
                SetCard *card = (SetCard *)resultFromFlip[i];
                
                NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
                [attributes setObject:card.contents forKey:NSForegroundColorAttributeName];
                [attributes setObject:card.color forKey:NSForegroundColorAttributeName];
            
                if ([card.color isEqualToString:@"red"])
                    [attributes setObject:[UIColor redColor] forKey:NSForegroundColorAttributeName];
                else if ([card.color isEqualToString:@"green"])
                    [attributes setObject:[UIColor greenColor] forKey:NSForegroundColorAttributeName];
                else if ([card.color isEqualToString:@"purple"])
                    [attributes setObject:[UIColor purpleColor] forKey:NSForegroundColorAttributeName];
            
                if ([card.shading isEqualToString:@"solid"])
                    [attributes setObject:@-5 forKey:NSStrokeWidthAttributeName];
                if ([card.shading isEqualToString:@"striped"])
                    [attributes addEntriesFromDictionary:@{
                         NSStrokeWidthAttributeName : @-5,
                         NSStrokeColorAttributeName : attributes[NSForegroundColorAttributeName],
                     NSForegroundColorAttributeName : [attributes[NSForegroundColorAttributeName] colorWithAlphaComponent:0.3]
                 }];
                if ([card.shading isEqualToString:@"open"])
                    [attributes setObject:@5 forKey:NSStrokeWidthAttributeName];
            
                NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:card.contents attributes:attributes];
                
                if(i>1){
                    [matchMessage appendAttributedString:[[NSAttributedString alloc] initWithString:@"&&"]];
                    [matchMessage appendAttributedString:attrString];
                }
                else{
                    [matchMessage appendAttributedString:attrString];
                }
            }
            
            if([score intValue] > 0){
                [matchMessage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" (%@ points score!)", score]]];
                
                [[self.game lastResultsMessages] addObject:matchMessage];
            }
            else{
                [matchMessage appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" (%@ points penalty!)", score]]];
                
                [[self.game lastResultsMessages] addObject:matchMessage];
            }
        }
    }
}

- (void)updateUI{
    for(UIButton *cardButton in self.cardButtons){
        SetCard *card = (SetCard *)[self.game cardAtIndex:[self.cardButtons indexOfObject:cardButton]];

        NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
        [attributes setObject:card.contents forKey:NSForegroundColorAttributeName];
        [attributes setObject:card.color forKey:NSForegroundColorAttributeName];
        
        if ([card.color isEqualToString:@"red"])
            [attributes setObject:[UIColor redColor] forKey:NSForegroundColorAttributeName];
        else if ([card.color isEqualToString:@"green"])
            [attributes setObject:[UIColor greenColor] forKey:NSForegroundColorAttributeName];
        else if ([card.color isEqualToString:@"purple"])
            [attributes setObject:[UIColor purpleColor] forKey:NSForegroundColorAttributeName];
        
        if ([card.shading isEqualToString:@"solid"])
            [attributes setObject:@-5 forKey:NSStrokeWidthAttributeName];
        if ([card.shading isEqualToString:@"striped"])
            [attributes addEntriesFromDictionary:@{
                     NSStrokeWidthAttributeName : @-5,
                     NSStrokeColorAttributeName : attributes[NSForegroundColorAttributeName],
                 NSForegroundColorAttributeName : [attributes[NSForegroundColorAttributeName] colorWithAlphaComponent:0.3]
             }];
        if ([card.shading isEqualToString:@"open"])
            [attributes setObject:@5 forKey:NSStrokeWidthAttributeName];
        
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:card.contents attributes:attributes];
        
        [cardButton setAttributedTitle:attrString forState:UIControlStateNormal];
        [cardButton setAttributedTitle:attrString forState:UIControlStateSelected];
        [cardButton setAttributedTitle:attrString forState:UIControlStateSelected|UIControlStateDisabled];
        
        cardButton.selected = card.isFaceUp;
        cardButton.enabled = !card.isUnplayable;
        cardButton.alpha = card.isUnplayable ? 0.3 : 1.0;
        
        if(!card.isFaceUp || card.isUnplayable){
            [cardButton setBackgroundColor:[[UIColor alloc] initWithRed:0.5 green:0.5 blue:0.5 alpha:0.3]];
        }
        else{
            [cardButton setBackgroundColor:[[UIColor alloc] initWithRed:0.5 green:0.5 blue:0.5 alpha:1.0]];
        }
    }
    
    [self.scoreLabel setText:[[NSString alloc] initWithFormat:@"Score: %d", [self.game score]]];
    
    if([[self.game getLastMatchMessage] isKindOfClass:[NSAttributedString class]]){
        NSAttributedString *lastMessage = (NSAttributedString *)[self.game getLastMatchMessage];
        [self.lastResultMessageLabel setAttributedText:lastMessage];
    }
}

- (IBAction)dealGame:(UIButton *)sender{
    for(UIButton *cardButton in self.cardButtons){
        cardButton.selected = false;
        cardButton.enabled = true;
        cardButton.alpha = 1.0;
    }
    
    self.game = nil;
    
    [self.scoreLabel setText:[[NSString alloc] initWithFormat:@"Score: %d", [self.game score]]];
    [self.lastResultMessageLabel setText:[self.game getLastMatchMessage]];
    self.flipCount = 0;
    [self updateUI];
}

- (void)setFlipCount:(int)flipCount{
    _flipCount = flipCount;
    self.flipLabel.text = [NSString stringWithFormat:@"Flips: %d", self.flipCount];
}

- (IBAction)flipCard:(UIButton *)sender {
    NSMutableArray *resultFromFlip = [self.game flipCardAtIndex:[self.cardButtons indexOfObject:sender]];
    [self generateMatchMessage:resultFromFlip];
    
    self.flipCount++;
    [self updateUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateUI];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
