//
//  Config.h
//  StanfordEDU
//
//  Created by Stephan Kristiansen on 23.06.13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#ifndef StanfordEDU_Config_h
#define StanfordEDU_Config_h

#define MATCH_BONUS 4
#define MISMATCH_PENALTY 2
#define FLIP_COST 1
#define TWO_CARD_MODE 0
#define THREE_CARD_MODE 1


#endif
