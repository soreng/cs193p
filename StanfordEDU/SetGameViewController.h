//
//  SetGameViewController.h
//  StanfordEDU
//
//  Created by Stephan Kristiansen on 12.07.13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetGameViewController : UIViewController

- (IBAction)dealGame:(UIButton *)sender;
- (IBAction)flipCard:(UIButton *)sender;

@end
