//
//  CardGameViewController.h
//  StanfordEDU
//
//  Created by Stephan Kristiansen on 21.06.13.
//  Copyright (c) 2013 Stephan Kristiansen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardGameViewController : UIViewController

- (IBAction)dealGame:(UIButton *)sender;
- (IBAction)flipCard:(UIButton *)sender;

@end
